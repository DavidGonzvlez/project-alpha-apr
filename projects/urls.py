from django.urls import path
from tasks.views import project_view

urlpatterns = [
    path("", project_view, name="list_projects"),
]
