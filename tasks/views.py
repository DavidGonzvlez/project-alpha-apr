from django.shortcuts import render
from projects.models import Project


def project_view(request):
    model_list = Project.objects.all()
    context = {"tasks": model_list}
    return render(request, "projects/list.html", context)


# Create your views here.
